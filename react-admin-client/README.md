# 1 安装redux相关依赖

* 命令：

```shell
yarn add redux redux-thunk react-redux redux-devtools-extension
```

# 2 安装axios

* 命令：

```shell
yarn add axios
```

# 3 安装ngprogress

```shell
yarn add nprogress
```

# 4 安装screenFull

```shell
yarn add screenfull
```

# 5 安装dayjsC

```shell
yarn add dayjs
```

# 6 安装jsonp（本项目中没有使用到）

```shell
yarn add jsonp
```

