import {SAVE_CATEGORY} from "../constant";

const category = (prevState = [], action) => {
    const {type, data} = action;

    switch (type) {
        case SAVE_CATEGORY:
            return [...data];
        default:
            return prevState
    }
}

export default category
