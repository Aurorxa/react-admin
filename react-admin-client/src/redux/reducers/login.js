import {SAVE_USER_AND_TOKEN, DELETE_USER_AND_TOKEN} from '../constant'
import store from 'store'
import {LOGIN_FLAG_USER, LOGIN_FLAG_TOKEN} from '../../config'

/*
* {user: {}, token: '', isLogin: false}
*/
//初始化的时候，从Local Storage中获取用户的信息
let user = store.get(LOGIN_FLAG_USER);
let token = store.get(LOGIN_FLAG_TOKEN);
const initState = {
    user: user || {},
    token: token || '',
    isLogin: user && token ? true : false
}

export default function login(prevState = initState, action) {
    const {type, data} = action;
    switch (type) {
        case SAVE_USER_AND_TOKEN:
            //将用户登录的信息保存到Local Storage中
            store.set(LOGIN_FLAG_USER, data.user);
            store.set(LOGIN_FLAG_TOKEN, data.token);
            //将用户登录的信息保存到redux中
            return {
                user: data.user,
                token: data.token,
                isLogin: true
            }
        case DELETE_USER_AND_TOKEN:
            //将用户登录的信息从Local Storage中删除
            store.remove(LOGIN_FLAG_USER);
            store.remove(LOGIN_FLAG_TOKEN);
            return {
                user: {},
                token: '',
                isLogin: false
            }
        default:
            return prevState;
    }
}
