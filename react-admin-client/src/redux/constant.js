/**
 * 该模块是用于定义action对象中type类型的常量值，目的只有一个：便于管理的同时，防止程序员单词写错
 */
//保存用户信息和token
export const SAVE_USER_AND_TOKEN = 'saveUserAndToken'
//删除用户信息和token
export const DELETE_USER_AND_TOKEN = 'deleteUserAndToken'
//保存标题
export const SAVE_TITLE = 'saveTitle'
//保存初始化或搜索的商品分页信息
export const SAVE_PRODUCT_LIST = 'saveProductList'
//保存商品分类
export const SAVE_CATEGORY = 'saveCategory'
