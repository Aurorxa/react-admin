import {SAVE_PRODUCT_LIST} from '../constant'

export const saveProductList = (data) => ({
    type: SAVE_PRODUCT_LIST,
    data
})
