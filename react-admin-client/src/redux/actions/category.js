import {SAVE_CATEGORY} from "../constant";

export const saveCategory = (data) => ({type: SAVE_CATEGORY, data})
